# LeaveSheet
![Android](https://shields.io/badge/platform-Android-%23092e40?style=flat&logo=android&logoColor=#30d780)
![Kotlin](https://shields.io/badge/language-Kotlin-%237F52FF?style=flat&logo=kotlin)
![version](https://img.shields.io/github/v/release/bqliang/LeaveSheet)
[![build](https://github.com/bqliang/LeaveSheet/actions/workflows/android.yml/badge.svg)](https://github.com/bqliang/LeaveSheet/actions/workflows/android.yml)

[Download](https://github.com/bqliang/LeaveSheet/releases/)
